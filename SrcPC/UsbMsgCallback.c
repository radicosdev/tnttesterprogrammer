
/*
example from
https://decibel.ni.com/content/docs/DOC-22836



see allso
https://msdn.microsoft.com/en-us/library/windows/desktop/aa363480%28v=vs.85%29.aspx

 WM_DEVICECHANGE message

*/



#include "windows.h" 
#include "dbt.h"
#include <cvirte.h>
#include <userint.h>
#include "sizepanel.h"
#include "toolbox.h"

static int panelHandle;

int flag;

DEV_BROADCAST_HDR * DeviceHeader;

int CVICALLBACK UsbMsgCallback   (int panelHandle, int message, unsigned int* wParam, unsigned int* lParam, void* callbackData);


int __stdcall WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{
	int res=0;

	if (InitCVIRTE (hInstance, 0, 0) == 0)
		return -1;          /* out of memory */
	if ((panelHandle = LoadPanel (0, "sizepanel.uir", PANEL)) < 0)
		return -1;
	res = InstallWinMsgCallback (panelHandle, WM_DEVICECHANGE, UsbMsgCallback, VAL_MODE_INTERCEPT, NULL, &flag);
	DisplayPanel (panelHandle);
	RunUserInterface ();
	DiscardPanel (panelHandle);
	RemoveWinMsgCallback (panelHandle, WM_CLOSE);

	return 0;
}


int CVICALLBACK QuitCallback (int panel, int control, int event, void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
        case EVENT_COMMIT:
          QuitUserInterface (0);
          break;
	}

	return 0;
}

int CVICALLBACK UsbMsgCallback (int panelHandle, int message, unsigned int* wParam, unsigned int* lParam, void* callbackData)
{ 
	int val=0;
	unsigned int size;
    unsigned int DeviceType;

	switch(*wParam)
	{
		case DBT_DEVICEARRIVAL:
			GetCtrlVal(panelHandle,PANEL_LED,&val);
			SetCtrlVal(panelHandle,PANEL_LED,!val);
			DeviceHeader=( DEV_BROADCAST_HDR*)(*lParam);
			size=DeviceHeader->dbch_size;
			DeviceType=DeviceHeader->dbch_devicetype;
			SetCtrlVal(panelHandle,PANEL_NUMERIC,DeviceType);
			break;
	}

    return 0;
}
