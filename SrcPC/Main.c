#include <userint.h>
#include "Main.h"

/////////////////////////////////////////////////////////////////////////////
//
//  Project: CVI_Test framework
//
//  Copyright L&V Design   - CH-1031 Mex
//
/////////////////////////////////////////////////////////////////////////////
/**
\mainpage
    This is the document for CVI test framework\n
    \n
    Brought to you by www.lvdesign.ch

    @file main.c
    Main module for CVI CVI_test system\n
    \n
    History:\n
      150107: Lang: Started\n
	  1602xx  V2-00	of PetPointer tests restart from here
	  191218: PW Adapt for Radicos AMM01 test bench
*/

#include <windows.h>
#include "dbt.h"
#include "toolbox.h"
#include <formatio.h>
#include "asynctmr.h"
#include <ansi_c.h>
#include <cvirte.h>
//#include "easytab.h"   PW/removed to compile in 64bits
#include "common.h"


//---------------------------------------------------------------------------
//  prototypes for local functions

void DisplayPassFail(void);
void Update_MainParams(void);


//---------------------------------------------------------------------------
/// <B>
/// [ Main: the meat of this affair! ]
/// </B>


int main (int argc, char *argv[])
{
    unsigned int     i;
    char    *p;
	char	buf[50];
    HANDLE  mutexHdl;
	int		ValToSet;
	int 	res=0;

	/// Allow only one instance running, using mutex
    // A named mutex is created
    mutexHdl = CreateMutex( NULL, TRUE, "CviTestMutex");

    // See if a previous mutex of the same name exists
    if (GetLastError() == ERROR_ALREADY_EXISTS)
    {
        // If so, notify the user and return
        MessageBox(NULL, TEXT("Sorry, application already running ..."),
                         TEXT("PePo Test"), MB_OK);
        return -1;
    }

    if (InitCVIRTE (0, argv, 0) == 0)
        return -1;  /* out of memory */

    /// Create panel
    /// - Load the main panel
    if ((mainPanel_handle = LoadPanel (0, "Main.uir", MAIN_PANEL)) < 0)
        return -1;

    
	/*
	/// - Load the tab sheet dialog
    /// - - retrieve the space reserved by the canvas
    tabCtrl = EasyTab_ConvertFromCanvas(mainPanel_handle, MAIN_PANEL_CANVAS);

    /// - - and load all child tabs here
    EasyTab_LoadPanels (mainPanel_handle, tabCtrl, 1, "Main.uir", __CVIUserHInst,
            P1_PROD     , &p1_handle,
            P2_DEBUG    , &p2_handle,
            //P3_SPC      , &p3_handle,
            //P4_CONFIG   , &p4_handle,
            //P5_HELP     , &p5_handle,
            0);

	*/
    if ((p1_handle = LoadPanel (mainPanel_handle, "Main.uir", P1_PROD)) < 0)
        return -1;
	
	GetCtrlAttribute(mainPanel_handle,MAIN_PANEL_CANVAS,ATTR_HEIGHT,&ValToSet);
	SetPanelAttribute(p1_handle,ATTR_HEIGHT ,ValToSet);
	
	GetCtrlAttribute(mainPanel_handle,MAIN_PANEL_CANVAS,ATTR_LEFT ,&ValToSet);
	SetPanelAttribute(p1_handle,ATTR_LEFT  ,ValToSet);
	
	GetCtrlAttribute(mainPanel_handle,MAIN_PANEL_CANVAS,ATTR_TOP ,&ValToSet);
	SetPanelAttribute(p1_handle,ATTR_TOP  ,ValToSet);
	
	GetCtrlAttribute(mainPanel_handle,MAIN_PANEL_CANVAS,ATTR_WIDTH ,&ValToSet);
	SetPanelAttribute(p1_handle,ATTR_WIDTH  ,ValToSet);


    TestInProgress = 0;						
	AllTestsResult = TEST_RUNNING;
	BoardInProcess	= 1;
    DisplayPassFail();

    /// Load parameter from "TestPlan"
	LoadTestPlan();

	DisplayPanel(mainPanel_handle);
    DisplayPanel(p1_handle);
	SetCtrlAttribute (mainPanel_handle, MAIN_PANEL_CONTINUOUSRUN, ATTR_VISIBLE , EnableExtraCtrl);

    /// Now, must enter the batch number reference
    do
    {
        BatchNumber[0] = 0;
        PromptPopup ("AMM01_TP", "Please enter production batch number", BatchNumber, 8);

        /// sanity check for PF entry (will resuse PF as datalog filename)
        p = BatchNumber;
        for (i = 0; i < sizeof(BatchNumber); i++)
        {
            if (isalnum(*p))
                p++;
            else
            {
                *p = 0;
                break;
            }
        }
    } while (BatchNumber[0] == 0);

    /// try to create logfile
    if (CreateLogFile() == 0)
    {
		//Start log from here for easy debug
		LogFileEnable   = 1;
		/// init test hardware
	    if (CVI_TestInit() != 0)
	    {
	        MessagePopup("AMM01_TP", "FATAL: Cannot initialize test hardware");
	        goto ExitTest;
	    }
	    if (CVI_InitExternalDevices() != 0)
	    {
			MessagePopup("AMM01_TP", "FATAL: Cannot initialize test device(s)" );
	        goto ExitTest;
	    }
		/// All right, let's go....
		ProcessSystemEvents();
        Update_MainParams();
        InitTimers();
		SetCtrlAttribute(mainPanel_handle,MAIN_PANEL_START_TEST , ATTR_VISIBLE,1);
        RunUserInterface();
        ShutdownTimers();
    }
    else
    {
        MessagePopup("CVI Test", "FATAL: Cannot create datalog file");
    }

    /// On exit, do house keeping before leaving the scene...
ExitTest:
    /// - shutdown test hardware
    CVI_TestCleanUp();

    /// - release panels
    DiscardPanel(p1_handle);
    //DiscardPanel(p2_handle);
    //DiscardPanel(p3_handle);
    //DiscardPanel(p4_handle);
    //DiscardPanel(p5_handle);
    DiscardPanel(mainPanel_handle);     // must be unloaded last

	RemoveWinMsgCallback (mainPanel_handle, WM_CLOSE);
    /// - release mutex of this application
    if (mutexHdl != NULL)
        CloseHandle(mutexHdl);

    return 0;
}

//---------------------------------------------------------------------------
/// Update main panel with info like product type, PF reference, Test Plan...

void Update_MainParams(void)
{
     SetCtrlVal(mainPanel_handle, MAIN_PANEL_REF_PRODUCT, ProductID);
     SetCtrlAttribute (mainPanel_handle, MAIN_PANEL_REF_PRODUCT, ATTR_TEXT_BGCOLOR, VAL_WHITE);

     SetCtrlVal(mainPanel_handle, MAIN_PANEL_REF_TESTPLAN, TestPlanID);
     SetCtrlAttribute (mainPanel_handle, MAIN_PANEL_REF_TESTPLAN, ATTR_TEXT_BGCOLOR, VAL_WHITE);

     SetCtrlVal(mainPanel_handle, MAIN_PANEL_REF_PF, BatchNumber);
     SetCtrlAttribute (mainPanel_handle, MAIN_PANEL_REF_PF, ATTR_TEXT_BGCOLOR, VAL_WHITE);
	 
	 SetActivePanel(mainPanel_handle);
	 //SetActiveCtrl(mainPanel_handle, MAIN_PANEL_START_TEST);
}

//---------------------------------------------------------------------------
/// Big display for "PASS/FAIL" info

void DisplayPassFail(void)
{
    char            InfoStr[6];
    unsigned long   Bck_color=VAL_WHITE, Txt_color;

    switch (AllTestsResult)
    {
    case TEST_RUNNING:
        strcpy(InfoStr, "Wait");
        Bck_color = VAL_YELLOW;
        Txt_color = VAL_BLACK;
        break;

    case TEST_PASSED:
        MessageBeep(MB_OK);
        strcpy(InfoStr, "Pass");
        Bck_color = VAL_GREEN;
        Txt_color = VAL_BLACK;
        break;

    case TEST_ABORTED:
        MessageBeep(MB_ICONHAND);
        strcpy(InfoStr, "Abort");
        Bck_color = VAL_RED;
        Txt_color = VAL_BLACK;
        break;

	case TEST_FAILED:
        MessageBeep(MB_ICONHAND);
        strcpy(InfoStr, "Fail");
        Bck_color = VAL_RED;
        Txt_color = VAL_BLACK;
        break;
    }

    SetCtrlVal(mainPanel_handle, MAIN_PANEL_STR_PASSFAIL, InfoStr);
    SetCtrlAttribute (mainPanel_handle, MAIN_PANEL_STR_PASSFAIL, ATTR_TEXT_BGCOLOR, Bck_color);

	
	SetTableCellVal      (p1_handle, P1_PROD_BOARD_TABLE, MakePoint(BoardInProcess, 1), InfoStr);  //row 1 = Pass/Fail
    SetTableCellAttribute(p1_handle, P1_PROD_BOARD_TABLE, MakePoint(BoardInProcess, 1), ATTR_TEXT_BGCOLOR, Bck_color);
	if ( AllTestsResult == TEST_RUNNING)
	{
		SetTableCellVal      (p1_handle, P1_PROD_BOARD_TABLE, MakePoint(BoardInProcess, 2), "");
		SetTableCellVal      (p1_handle, P1_PROD_BOARD_TABLE, MakePoint(BoardInProcess, 3), "");
	}
	else
	{
		SetTableCellVal      (p1_handle, P1_PROD_BOARD_TABLE, MakePoint(BoardInProcess, 2), AllTestInError);
		SetTableCellVal      (p1_handle, P1_PROD_BOARD_TABLE, MakePoint(BoardInProcess, 3), FirstValInError);
	}
	
}

//---------------------------------------------------------------------------
/// Called when "Exit Program" pressed

int CVICALLBACK Done_CB (int panel, int control, int event,
                         void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        /// Prevent exiting program while a test is in progress
        if (TestInProgress == 1)
        {
            MessagePopup("WARNING", "Cannot exit program while test in progress");
            return 0;
        }

        QuitUserInterface(0);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------
/// Called when "Abort Test" pressed

int CVICALLBACK AbortTest_CB (int panel, int control, int event,
                              void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        /// will abort the next pending test, if any
        AbortTest = 1;
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------
int TestAllBoards(void)
{
	AbortTest = 0;
	BoardInProcess	= 1;
    Clear_BoardsTestResults(); 
	TestInProgress  = 1;
	do
    {
		GetCtrlVal(MAIN_PANEL,MAIN_PANEL_TOGGLEBUTTON,&SerieIdToPrg);
		sprintf(NodeId,"%X\0", (BoardInProcess - 1 + (SerieIdToPrg *8)));
		SelectBoardForMeasurements(BoardInProcess);    
    	AllTestsResult  = TEST_RUNNING;
    	DisplayPassFail();
		send_com(MODEM,"mcf,9,0\r");   //Set modem to send data only after RMEAS cmd
    	// now chain all predefined tests
    	CVI_TestAll();
		send_com(MODEM,"RAZ\r");   //Reset modem to be shure ist stoped (in case of error)
    	// and display results
    	DisplayPassFail();
        
		BoardInProcess++;
		if (AbortTest != 0)
			break;
    } while (BoardInProcess <= MAX_BOARD);
	if (SerieIdToPrg == 0)
	{
		SetCtrlVal(MAIN_PANEL,MAIN_PANEL_TOGGLEBUTTON,1);
	}
	else
	{
		SetCtrlVal(MAIN_PANEL,MAIN_PANEL_TOGGLEBUTTON,0);
	}
    TestInProgress  = 0;
	return 0;
}
//---------------------------------------------------------------------------
///  Called when "Start" pressed

int CVICALLBACK StartTest_CB (int panel, int control, int event,
                              void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
        {
            /// prepare to run all tests on all boards
            LogScreenEnable = 1;
            LogFileEnable   = 1;

            /// now chain all predefined tests
            TestAllBoards();

        }
        break;
    }
    return 0;
}


 //---------------------------------------------------------------------------
void RunContinuously(void)
{
    AbortTest = 0;
	TestInProgress  = 1;
	do
    {
		BoardInProcess	= 1;
	    Clear_BoardsTestResults(); 
		do
	    {
	        SelectBoardForMeasurements(BoardInProcess);    
	    	AllTestsResult  = TEST_RUNNING;
	    	DisplayPassFail();

	    	// now chain all predefined tests
	    	CVI_TestAll();

	    	// and display results
	    	DisplayPassFail();
        
			BoardInProcess++;
			if (AbortTest != 0)
				break;
	    } while (BoardInProcess <= MAX_BOARD);
	} while (AbortTest == 0 );
	TestInProgress  = 0;
}

 //---------------------------------------------------------------------------

int CVICALLBACK ContRun_CB (int panel, int control, int event,
							void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			RunContinuously();
			break;
	}
	return 0;
}

int CVICALLBACK SetIdSerie (int panel, int control, int event,
							void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			GetCtrlVal(panel,control,&SerieIdToPrg);
			break;
	}
	return 0;
}
