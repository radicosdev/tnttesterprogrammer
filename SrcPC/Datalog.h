/////////////////////////////////////////////////////////////////////////////
//
//  Project: CVI_Test framework
//
//  Copyright L&V Design   - CH-1031 Mex
//
/////////////////////////////////////////////////////////////////////////////
/**
    @file datalog.h
    \n
    History:\n
      150107: Lang: Started\n
      150612: PhW:  Updated to PePo\n
*/

int CreateLogFile(void);
int BackupLogFile(void);
int LogString(char *str);	   // __LogString(char *str);
int LogHeader(void);
int LogResults(void);
extern char	DescriptLineTests[1500];



