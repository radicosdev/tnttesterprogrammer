/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  MAIN_PANEL                       1
#define  MAIN_PANEL_START_TEST            2       /* control type: command, callback function: StartTest_CB */
#define  MAIN_PANEL_STR_PASSFAIL          3       /* control type: string, callback function: (none) */
#define  MAIN_PANEL_BUTT_ABORT            4       /* control type: command, callback function: AbortTest_CB */
#define  MAIN_PANEL_DONE                  5       /* control type: command, callback function: Done_CB */
#define  MAIN_PANEL_REF_PRODUCT           6       /* control type: string, callback function: (none) */
#define  MAIN_PANEL_REF_PF                7       /* control type: string, callback function: (none) */
#define  MAIN_PANEL_REF_SERIALNB          8       /* control type: string, callback function: (none) */
#define  MAIN_PANEL_REF_TESTPLAN          9       /* control type: string, callback function: (none) */
#define  MAIN_PANEL_CANVAS                10      /* control type: canvas, callback function: (none) */
#define  MAIN_PANEL_BUTT_TGLREL           11      /* control type: textButton, callback function: Wash_Relays */
#define  MAIN_PANEL_CONTINUOUSRUN         12      /* control type: command, callback function: ContRun_CB */
#define  MAIN_PANEL_TOGGLEBUTTON          13      /* control type: textButton, callback function: SetIdSerie */

#define  P1_PROD                          2
#define  P1_PROD_TEST_TABLE               2       /* control type: table, callback function: (none) */
#define  P1_PROD_TEST_STS                 3       /* control type: string, callback function: (none) */
#define  P1_PROD_TIME_ELAPSED             4       /* control type: numeric, callback function: (none) */
#define  P1_PROD_BOARD_TABLE              5       /* control type: table, callback function: (none) */

#define  P2_DEBUG                         3
#define  P2_DEBUG_DBG_TEST_1              2       /* control type: command, callback function: DbgTest1_CB */
#define  P2_DEBUG_DBG_TEST_2              3       /* control type: command, callback function: DbgTest2_CB */
#define  P2_DEBUG_DBG_TEST_3              4       /* control type: command, callback function: DbgTest3_CB */
#define  P2_DEBUG_DBG_TEST_4              5       /* control type: command, callback function: DbgTest4_CB */
#define  P2_DEBUG_DBG_TEST_5              6       /* control type: command, callback function: DbgTest5_CB */
#define  P2_DEBUG_DBG_TEST_6              7       /* control type: command, callback function: DbgTest6_CB */
#define  P2_DEBUG_DBG_TEST_7              8       /* control type: command, callback function: DbgTest7_CB */
#define  P2_DEBUG_DBG_TEST_8              9       /* control type: command, callback function: DbgTest8_CB */
#define  P2_DEBUG_DBG_TEST_9              10      /* control type: command, callback function: DbgTest9_CB */
#define  P2_DEBUG_DBG_TEST_10             11      /* control type: command, callback function: DbgTest10_CB */
#define  P2_DEBUG_DBG_TEST_11             12      /* control type: command, callback function: DbgTest11_CB */
#define  P2_DEBUG_DBG_TEST_12             13      /* control type: command, callback function: DbgTest12_CB */
#define  P2_DEBUG_DBG_TEST_13             14      /* control type: command, callback function: DbgTest13_CB */
#define  P2_DEBUG_DBG_TEST_14             15      /* control type: command, callback function: DbgTest14_CB */
#define  P2_DEBUG_DBG_TEST_15             16      /* control type: command, callback function: DbgTest15_CB */
#define  P2_DEBUG_DBG_TEST_16             17      /* control type: command, callback function: DbgTest16_CB */
#define  P2_DEBUG_DBG_TEST_17             18      /* control type: command, callback function: DbgTest17_CB */
#define  P2_DEBUG_DBG_TEST_18             19      /* control type: command, callback function: DbgTest18_CB */
#define  P2_DEBUG_DBG_TEST_19             20      /* control type: command, callback function: DbgTest19_CB */
#define  P2_DEBUG_DBG_TEST_20             21      /* control type: command, callback function: DbgTest20_CB */
#define  P2_DEBUG_DBG_TEST_41             22      /* control type: command, callback function: DbgTest41_CB */
#define  P2_DEBUG_DBG_TEST_42             23      /* control type: command, callback function: DbgTest42_CB */
#define  P2_DEBUG_DBG_TEST_43             24      /* control type: command, callback function: DbgTest43_CB */
#define  P2_DEBUG_DBG_TEST_44             25      /* control type: command, callback function: DbgTest44_CB */
#define  P2_DEBUG_DBG_TEST_45             26      /* control type: command, callback function: DbgTest45_CB */
#define  P2_DEBUG_DBG_TEST_31             27      /* control type: command, callback function: DbgTest31_CB */
#define  P2_DEBUG_DBG_TEST_32             28      /* control type: command, callback function: DbgTest32_CB */
#define  P2_DEBUG_DBG_TEST_33             29      /* control type: command, callback function: DbgTest33_CB */
#define  P2_DEBUG_DBG_TEST_34             30      /* control type: command, callback function: DbgTest34_CB */
#define  P2_DEBUG_DBG_TEST_35             31      /* control type: command, callback function: DbgTest35_CB */
#define  P2_DEBUG_DBG_TEST_46             32      /* control type: command, callback function: DbgTest46_CB */
#define  P2_DEBUG_DBG_TEST_47             33      /* control type: command, callback function: DbgTest47_CB */
#define  P2_DEBUG_DBG_TEST_48             34      /* control type: command, callback function: DbgTest48_CB */
#define  P2_DEBUG_DBG_TEST_49             35      /* control type: command, callback function: DbgTest49_CB */
#define  P2_DEBUG_DBG_TEST_50             36      /* control type: command, callback function: DbgTest50_CB */
#define  P2_DEBUG_DBG_TEST_36             37      /* control type: command, callback function: DbgTest36_CB */
#define  P2_DEBUG_DBG_TEST_37             38      /* control type: command, callback function: DbgTest37_CB */
#define  P2_DEBUG_DBG_TEST_38             39      /* control type: command, callback function: DbgTest38_CB */
#define  P2_DEBUG_DBG_TEST_39             40      /* control type: command, callback function: DbgTest39_CB */
#define  P2_DEBUG_DBG_TEST_40             41      /* control type: command, callback function: DbgTest40_CB */
#define  P2_DEBUG_DBG_TEST_21             42      /* control type: command, callback function: DbgTest21_CB */
#define  P2_DEBUG_DBG_TEST_22             43      /* control type: command, callback function: DbgTest22_CB */
#define  P2_DEBUG_DBG_TEST_23             44      /* control type: command, callback function: DbgTest23_CB */
#define  P2_DEBUG_DBG_TEST_24             45      /* control type: command, callback function: DbgTest24_CB */
#define  P2_DEBUG_DBG_TEST_25             46      /* control type: command, callback function: DbgTest25_CB */
#define  P2_DEBUG_DBG_TEST_26             47      /* control type: command, callback function: DbgTest26_CB */
#define  P2_DEBUG_DBG_TEST_27             48      /* control type: command, callback function: DbgTest27_CB */
#define  P2_DEBUG_DBG_TEST_28             49      /* control type: command, callback function: DbgTest28_CB */
#define  P2_DEBUG_DBG_TEST_29             50      /* control type: command, callback function: DbgTest29_CB */
#define  P2_DEBUG_DBG_TEST_30             51      /* control type: command, callback function: DbgTest30_CB */
#define  P2_DEBUG_RELOAD_TESTPLAN         52      /* control type: command, callback function: DbgReloadTestPlan_CB */
#define  P2_DEBUG_SAVE_LOG                53      /* control type: command, callback function: SaveLogScreen_CB */
#define  P2_DEBUG_CLEAR_LOG               54      /* control type: command, callback function: ClearLogScreen_CB */
#define  P2_DEBUG_TESL_ALL                55      /* control type: command, callback function: DbgTestAll_CB */
#define  P2_DEBUG_LOG_SCREEN              56      /* control type: textBox, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_1              57      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_2              58      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_3              59      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_4              60      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_5              61      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_41             62      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_42             63      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_43             64      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_44             65      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_45             66      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_6              67      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_7              68      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_8              69      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_9              70      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_10             71      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_46             72      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_47             73      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_48             74      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_49             75      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_50             76      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_31             77      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_32             78      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_33             79      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_34             80      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_35             81      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_36             82      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_37             83      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_38             84      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_39             85      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_40             86      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_11             87      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_12             88      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_13             89      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_14             90      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_15             91      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_16             92      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_17             93      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_18             94      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_19             95      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_20             96      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_21             97      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_22             98      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_23             99      /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_24             100     /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_25             101     /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_26             102     /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_27             103     /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_28             104     /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_29             105     /* control type: string, callback function: (none) */
#define  P2_DEBUG_LBL_TEST_30             106     /* control type: string, callback function: (none) */

#define  P3_SPC                           4

#define  P4_CONFIG                        5

#define  P5_HELP                          6
#define  P5_HELP_HELPBOX                  2       /* control type: textBox, callback function: (none) */
#define  P5_HELP_TEXTMSG                  3       /* control type: textMsg, callback function: (none) */
#define  P5_HELP_TEXTMSG_4                4       /* control type: textMsg, callback function: (none) */
#define  P5_HELP_TEXTMSG_2                5       /* control type: textMsg, callback function: (none) */


     /* Control Arrays: */

          /* (no control arrays in the resource file) */


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */

int  CVICALLBACK AbortTest_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ClearLogScreen_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ContRun_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgReloadTestPlan_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest10_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest11_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest12_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest13_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest14_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest15_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest16_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest17_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest18_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest19_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest1_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest20_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest21_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest22_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest23_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest24_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest25_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest26_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest27_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest28_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest29_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest2_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest30_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest31_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest32_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest33_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest34_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest35_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest36_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest37_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest38_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest39_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest3_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest40_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest41_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest42_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest43_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest44_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest45_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest46_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest47_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest48_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest49_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest4_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest50_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest5_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest6_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest7_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest8_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTest9_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DbgTestAll_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK Done_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK SaveLogScreen_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK SetIdSerie(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK StartTest_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK Wash_Relays(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
