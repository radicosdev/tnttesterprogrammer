/////////////////////////////////////////////////////////////////////////////
//
//  Project: CVI_Test framework
//
//  Copyright L&V Design   - CH-1031 Mex
//
/////////////////////////////////////////////////////////////////////////////
/**
    @file Timer.h
    \n
    History:\n
      150107: Lang: Started\n
      150612: PhW:  Updated to PePo\n
*/

int InitTimers(void);
int ShutdownTimers(void);
int CVICALLBACK Timer1_CB (int panel, int control, int event,
                           void *callbackData, int eventData1, int eventData2);
int CVICALLBACK Timer2_CB (int panel, int control, int event,
                           void *callbackData, int eventData1, int eventData2);

