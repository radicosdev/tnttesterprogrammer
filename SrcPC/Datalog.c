/////////////////////////////////////////////////////////////////////////////
//
//  Project: CVI_Test framework
//
//  Copyright L&V Design   - CH-1031 Mex
//
/////////////////////////////////////////////////////////////////////////////
/**
    @file Datalog.c
    Datalog of all test results, according to VSA specifications\n
    \n
    History:\n
      150107: Lang: Started\n
*/

#include <windows.h>
#include <utility.h>
#include <ansi_c.h>
#include "common.h"

char	DescriptLineTests[1500];


static char    LogFileName[260];
static FILE    *fpLog;

//---------------------------------------------------------------------------

int GetLogFileName(void)
{
    sprintf(LogFileName, "%s/%s.%s", TestLogFilePath, BatchNumber, LogFileEXT);
    return 0;
}

//---------------------------------------------------------------------------

int CreateLogFile(void)
{
	int	TheOldvalue;

	GetLogFileName();
	TheOldvalue=SetBreakOnLibraryErrors(0);
	fpLog = fopen (LogFileName, "r");   //a+
	SetBreakOnLibraryErrors(TheOldvalue);
	if (fpLog == NULL)
    {
        fpLog = fopen (LogFileName, "a+");
    	if (fpLog == NULL)
		{
		MessagePopup("AMM01_TP", "FATAL: Error creating log file");
        return -1;
		}
		else
		{
 			if (fprintf(fpLog, "%s", DescriptLineTests) < 0)
    		{
        	MessagePopup("AMM01_TP", "FATAL: Error writing log file");
        	return -1;
    		}
		}
	}
    fclose(fpLog);
    return 0;
}

//---------------------------------------------------------------------------

int BackupLogFile(void)
{
    return 0;
}

//---------------------------------------------------------------------------

int LogString(char *str)
{
    int     errcode = 0;

    // no datalog in debug mode
    if (LogFileEnable == 0)
        return 0;

    fpLog = fopen (LogFileName, "a+");
    if (fpLog == NULL)
    {
        MessagePopup("AMM01_TP", "FATAL: Error opening log file");
        return -1;
    }

    if (fprintf(fpLog, "%s\n", str) < 0)
    {
        MessagePopup("AMM01_TP", "FATAL: Error writing log file");
        errcode = -1;
    }
	ProcessSystemEvents ();
    fclose(fpLog);
	ProcessSystemEvents ();
    return errcode;
}

//---------------------------------------------------------------------------

int LogHeader(void)
{
    return 0;
}

//---------------------------------------------------------------------------

int LogResults(void)
{
    return 0;
}

//---------------------------------------------------------------------------

