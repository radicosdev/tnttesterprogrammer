/////////////////////////////////////////////////////////////////////////////
//
//  Project: CVI_Test framework
//
//  Copyright L&V Design   - CH-1031 Mex
//
/////////////////////////////////////////////////////////////////////////////
/**
    @file Timer.c
    \n
    History:\n
      150107: Lang: Started\n
      120207: Lang: Use async timers (std timer incompatible with ext application)\n
*/

#include <windows.h>
#include "asynctmr.h"
#include <ansi_c.h>
#include "common.h"

//---------------------------------------------------------------------------

static int mTimer1, mTimer2;

int InitTimers(void)
{
    mTimer1 = NewAsyncTimer (0.1, -1, 1, Timer1_CB, NULL);
    if (mTimer1 <= 0)
    {
        MessagePopup("Async Timer", "Async timer1 could not be created");
        mTimer1 = 0;
        return -1;
    }

    mTimer2 = NewAsyncTimer (1.0, -1, 1, Timer2_CB, NULL);
    if (mTimer2 <= 0)
    {
        MessagePopup("Async Timer", "Async timer2 could not be created");
        mTimer2 = 0;
        return -1;
    }
    return 0;
}

//---------------------------------------------------------------------------

int ShutdownTimers(void)
{
    if (DiscardAsyncTimer (mTimer1) < 0)
    {
        MessagePopup("Async Timer", "Async timer1 could not be discarded");
        mTimer1 = 0;
        return -1;
    }

    if (DiscardAsyncTimer (mTimer2) < 0)
    {
        MessagePopup("Async Timer", "Async timer2 could not be discarded");
        mTimer2 = 0;
        return -1;
    }
    return 0;
}

//---------------------------------------------------------------------------
///  Timer1: 0.1sec resolution

int CVICALLBACK Timer1_CB (int panel, int control, int event,
                           void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_TIMER_TICK:
        // reserved for rs232 timeout
        if (Timer1_WatchDog1)
           Timer1_WatchDog1--;

        // reserved for external application watchdog
        if (Timer1_WatchDog2)
            Timer1_WatchDog2--;

        // reserved for ???
        if (Timer1_WatchDog3)
            Timer1_WatchDog3--;

        // Reserved for ???
        if (Timer1_WatchDog4)
            Timer1_WatchDog4--;

        break;
    }
    return 0;
}

//---------------------------------------------------------------------------
///  Timer2: 1sec resolution

int CVICALLBACK Timer2_CB (int panel, int control, int event,
                           void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_TIMER_TICK:
        if (TestInProgress)
        {
            if (Timer2_TestTime < 400000)     // should be enough!!!
                Timer2_TestTime++;
            SetCtrlVal (p1_handle, P1_PROD_TIME_ELAPSED, Timer2_TestTime);
        }
        else
        {
            Timer2_TestTime = 0;
        }
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------



