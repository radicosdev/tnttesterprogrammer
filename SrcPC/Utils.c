/////////////////////////////////////////////////////////////////////////////
//
//  Project: CVI_Test framework
//
//  Copyright L&V Design   - CH-1031 Mex
//
/////////////////////////////////////////////////////////////////////////////
/**
    @file Utils.c
    !!! Fouzytout !!!\n
    \n
    History:\n
      150107: Lang: Started\n
*/

#include <ansi_c.h>
#include <windows.h>
#include <utility.h>
#include "common.h"

//---------------------------------------------------------------------------
//  Keep Windows alive

void __MyPumpMessage(void)
{
    MSG msg;
    while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
}

//---------------------------------------------------------------------------

BYTE gethex(char *ptr )
{
    BYTE h, l;

    h = *(ptr++);
    l = *ptr;
    h = toupper(h) - '0'; if ( h > 9 ) h -= 7;
    l = toupper(l) - '0'; if ( l > 9 ) l -= 7;
    return h * 16 + l;
}

//---------------------------------------------------------------------------
// STOI.C - Convert string to integer, more powerful version of atoi.
//
// If string starts with 0x it is interpreted as a hex number,
// else if it starts with a 0 it is octal, else it is decimal.
// Conversion stops on encountering the first character which is not
// a digit in the indicated radix.
//
// adapted from Dr Dobb Journal
// Add binary extension like 0b10100010   - lang 210394
// modified to return long int
// add single char entry
//---------------------------------------------------------------------------

long int stoi(char *instr)
{
    long int num = 0L;
    int     sign = 1;
    char    *str;

    str = instr;
    // skip unwanted space, tab.. and so one.
    while (*str == ' ' || *str == '\t' || *str == '\n')
        str++;

    // Make sure there is still something to deal with
    if (*str == 0)
        return 0;

    // ascii char?
    if (*str == '\'')
    {
        str++;
        if (__isascii(*str))    return (long) *str;
        else                    return 0L;
    }

    // sign?
    if (*str == '-')
    {
        sign = -1;
        str++;
    }

    // octal or hex or bin
    if (*str == '0')
    {
        // hex char
        ++str;
        if (*str == 'x' || *str == 'X')
        {
            str++;
            while (('0' <= *str && *str <= '9') ||
                   ('a' <= *str && *str <= 'f') ||
                   ('A' <= *str && *str <= 'F'))
            {
                num *= 16;
                num += ('0' <= *str && *str <= '9') ?
                        *str - '0' :
                        toupper(*str) - 'A' + 10;
                str++;
            }
        }

        // binary
        else if (*str == 'b' || *str == 'B')
        {
            str++;
            while (*str == '0' || *str == '1')
            {
                num <<= 1;
                if (*str == '1')
                    num += 1;
                str++;
            }
        }

        // octal
        else
        {
            while ('0' <= *str && *str <= '7')
            {
                num *= 8;
                num += *str++ - '0';
            }
        }
    }

    // decimal
    else
    {
        while ('0' <= *str && *str <= '9')
        {
            num *= 10;
            num += *str++ - '0';
        }
    }
    return (num * sign);
}

//---------------------------------------------------------------------------

int Launch_16bit_App(char *PathName, int MaxTime)
{
    int         g_exeHandle;
    HWND        mainwindow;
    int         errcode;
    char    cmdline[250];

    // Put itself as topmost against other windows
    mainwindow = FindWindow(NULL, "PetPointer Test System V1.00");
    if(mainwindow == NULL)
    {
        MessagePopup ("INTERNAL ERROR", "Please contact PePo test support");
        return -1;
    }

    SetWindowPos(mainwindow, HWND_TOPMOST, 0,0,0,0,SWP_NOMOVE| SWP_NOSIZE);

    sprintf(cmdline, "cmd /c start /WAIT /MIN %s", PathName); 
    if (LaunchExecutableEx (cmdline, LE_SHOWMINIMIZED, &g_exeHandle) != 0)
    {
        errcode = -1;
        goto LaunchDone;
    }

    Timer1_WatchDog1 = MaxTime;
    do
    {
        if (Timer1_WatchDog1 == 0)
        {
            // Big trouble
            TerminateExecutable    (g_exeHandle);
            RetireExecutableHandle (g_exeHandle);
            errcode = -1;
            goto LaunchDone;
        }
        __MyPumpMessage();
    } while (!ExecutableHasTerminated (g_exeHandle));

    RetireExecutableHandle (g_exeHandle);
    errcode = 0;

LaunchDone:
    /// release topmost position
    SetWindowPos(mainwindow, HWND_NOTOPMOST, 0,0,0,0,SWP_NOMOVE| SWP_NOSIZE);
    return errcode;
}

//---------------------------------------------------------------------------

int Launch_32bit_App(char *PathName, int MaxTime)
{
    int         g_exeHandle;
    HWND        mainwindow;
    int         errcode;

    // Put itself as topmost against other windows
    mainwindow = FindWindow(NULL, "PetPointer Test System V1.00");
    if(mainwindow == NULL)
    {
        MessagePopup ("INTERNAL ERROR", "Please contact PePo Test support");
        return -1;
    }

    SetWindowPos(mainwindow, HWND_TOPMOST, 0,0,0,0,SWP_NOMOVE| SWP_NOSIZE);

    if (LaunchExecutableEx (PathName, LE_SHOWMAXIMIZED, &g_exeHandle) != 0)
    {
        errcode = -1;
        goto LaunchDone;
    }

    Timer1_WatchDog1 = MaxTime;
    do
    {
        if (Timer1_WatchDog1 == 0)
        {
            // Big trouble
            TerminateExecutable    (g_exeHandle);
            RetireExecutableHandle (g_exeHandle);
            errcode = -1;
            goto LaunchDone;
        }
        __MyPumpMessage();
    } while (!ExecutableHasTerminated (g_exeHandle));

    RetireExecutableHandle (g_exeHandle);
    errcode = 0;

LaunchDone:
    /// release topmost position
    SetWindowPos(mainwindow, HWND_NOTOPMOST, 0,0,0,0,SWP_NOMOVE| SWP_NOSIZE);
    return errcode;
}

//---------------------------------------------------------------------------




















