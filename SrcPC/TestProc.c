#include <userint.h>
#include "Main.h"

/////////////////////////////////////////////////////////////////////////////
//
//  Project: CVI_Test framework
//
//  Copyright L&V Design   - CH-1031 Mex
//
/////////////////////////////////////////////////////////////////////////////
/**
    @file TestProc.c
    All real CVI tests defined here\n
    \n
    History:\n
      150107: Lang: Started\n
	  150714: PhW:  PePo\n
*/

const	double 	WaitRelayStable 		= 0.1;	//Optimise 1rst
const	double 	WaitDMMExecutComm 		= 0.1;	//Optimise 2nd	  
const	double 	WaitValueStableOnDMM 	= 0.7;	//Optimise 3rd
const	double 	WaitIvalStableOnDMM 	= 0.5;	//Optimise 3rd
const	double	WaitUAlimStable			= 0.5;
const	double	WaitComDispo			= 1;	//3
const	double	ResetNumato5			= 0.5;
const	double	WaitAddFirstTime		= 1;

#include <windows.h>
#include "toolbox.h"
#include <formatio.h>
#include <rs232.h>
#include <utility.h>
#include <ansi_c.h>
#include "common.h"
#include "instruments.h"
#include <float.h>
#include <stdio.h>


int CurrTest = 0;

#define NUMATO1  com11_port
#define NUMATO2  com12_port
#define NUMATO3  com13_port
#define NUMATO4  com14_port
#define NUMATO5  com15_port
#define MODEM    com16_port
//#define STLINK   com17_port
#define DMM      com18_port


//Define relais position on NUMATO1/2/3/4 fonction DUT to connect
#define SelectModule1_3_5_7		"00003ffc" // (0x00003ffc)
#define SelectModule2_4_6_8		"03ffc000" //(0x03ffc000)



//Define relais position on NUMATO5 fonction of type of measurement or connection
//DMM and Alim
#define RelInVDMMToGnd		"0"
#define	RelIn0VDmmToTp		"1"
#define	RelIn0VDMMToVAlim	"2"
#define	RelModemOutToIDmm	"3"
#define	RelModemOutToVAlim	"T"

#define	RelModemToLS		"4"
#define	RelModemToU			"5"
#define	RelModemToRS		"6"

//Res conctions
#define	RelResToGnd			"E"
#define	RelResTo5V			"8"
#define	RelResToTP16		"G"
#define	RelResToTP17		"F"

//Programer STLINK connections
#define	RelProgToGnd		"D"
#define	RelProgToRVO		"A"
#define	RelProgToSres		"H"
#define	RelProgToSwim		"J"
#define	RelProgToNu			"I"


#define	RelNuToResCharge	"C"

#define RelMesRs			"7"
#define RelMesRvo			"9"
#define RelMesLs			"B"
#define RelMesSres			"K"
#define RelMesNu			"L"
#define RelMesSwim			"M"
#define RelMesVsei			"N"
#define RelMesVdd			"O"
#define RelMesScOut			"P"
#define RelMesOos			"Q"
#define RelMesLGRD			"R"
#define RelMesLGRS			"S"

#define RelUToGnd			"U"


int     retcode;
char    cmdstr[100];
long    sn;


char    fullRes[1500];

float 	measurement=0;
int		DoRetry = 0; //Out test retry (in CVI_TestAll(()) >0 => make a retry

CVIAbsoluteTime TheTime;
CVIAbsoluteTime TheOldTime;
CVITimeInterval DeltaTime;
	
//---------------------------------------------------------------------------
//return -1 if error sending

int send_com(int portNo, char str[])
{
    FlushInQ  (portNo);
    FlushOutQ (portNo);
    if (ComWrt(portNo, str, strlen(str)) <=0)
	{
		return -1;
	}
	else
	{
	    if (portNo <= NUMATO5)
		{
			  DelayWithEventProcessing(WaitRelayStable); 
		}
	
		else if (portNo==DMM)
	    {
	       DelayWithEventProcessing(WaitDMMExecutComm);
	    }
		//To be shure all writed
		while (GetOutQLen(portNo) > 0)
		{
			DelayWithEventProcessing(0.2);	
		} 
	    return 0;
	}
}


//---------------------------------------------------------------------------
float readDMM(int filter)
{
    char c;
    char *ptr, *ptr2;
	char    response[100]="";

    DelayWithEventProcessing(WaitValueStableOnDMM);
	//Perform immediate measurement and returns a numerical value.
    send_com(DMM,"FETCH?\r");
    //                          12345678901234567890
    // filter = 1 YES: the respose will be FETCH?\r-1.234E-4 with NO <cr> at the end
    // so have to read 20 bytes or until timeout
	retcode= ComRdTerm (DMM, response, 20, 0x0a);   // wait for \n (LF)

    ptr=strstr(response, "\r");
    ptr++;

    if(filter==1)
    {
        ptr2 = ptr;

        while ((c = *ptr2) != 0)
        {
            if (    (c == 'e')
                 || (c == 'E')
                 || (c == '+')
                 || (c == '-')
                 || (c == '.')
                 || ((c >= '0') && (c <= '9'))
               )
            {
                ptr2++;
            }
            else
            {
                *ptr2 = 0;
                break;
            }
        }
    }

    //MessagePopup ("DMM value", ptr);
    return (float)atof(ptr);
}

//---------------------------------------------------------------------------
void set_curr_measurement(void)
{
    char    buf[30]="";

    //Prepare DMM
    send_com(DMM,":FUNC CURR:DC;:CURR:DC:RANG MIN;:CURR:DC:NPLC 1\r");
    DelayWithEventProcessing(WaitIvalStableOnDMM);								  
    //Set relays to measure current
    sprintf(buf,"relay on %s\r", RelIn0VDMMToVAlim);  //2
    //send_com(NUMATO5, buf);
    sprintf(buf,"%srelay on %s\r", buf,RelModemOutToIDmm);  //3
    send_com(NUMATO5, buf);
	DelayWithEventProcessing(0.5);
    sprintf(buf,"relay on %s\r",RelModemOutToVAlim); //TR
    send_com(NUMATO5, buf);
    DelayWithEventProcessing(WaitIvalStableOnDMM);	
	ProcessSystemEvents();
	return;
}

//---------------------------------------------------------------------------
void set_volt_measurement(void)
{
    char    buf[30]="";
    //Reset relays  voltage measurement
    sprintf(buf,"relay off %s\r",RelModemOutToVAlim);
    send_com(NUMATO5, buf);
	DelayWithEventProcessing(0.5);
    sprintf(buf,"relay off %s\r", RelModemOutToIDmm);
    //send_com(NUMATO5, buf);
    sprintf(buf,"%srelay off %s\r", buf, RelIn0VDMMToVAlim);
    send_com(NUMATO5, buf);
	send_com(DMM,":FUNC VOLT:DC;:VOLT:DC:RANG 20\r");
	ProcessSystemEvents();
	return;
}

//---------------------------------------------------------------------------
float dmm_meas_curr(void)
{
    float value=0;
    char    buf[30]="";

    //Set relays to measure current
//Lang:Danger
//  relay on 2 ==> Alim_V+ connect to DMM_V-
//  relay on 3 ==> microcoupure sur Alim_V+
//Lang:has warned to use 1 more relay to prevlent this problem

//Lang: Vdrop max 0.3V @200mA. probably lost 0.1V during programmation
    // send_com(DMM,":FUNC CURR:DC;:CURR:DC:RANG 0.2;:CURR:DC:NPLC 0.1\r");  Modifi� par Thanh 28.08.15
    send_com(DMM,":FUNC CURR:DC;:CURR:DC:RANG 0.2;:CURR:DC:NPLC 1\r");
    DelayWithEventProcessing(WaitIvalStableOnDMM);								  
    sprintf(buf,"relay on %s\r", RelIn0VDMMToVAlim);  //2
    send_com(NUMATO5, buf);
    sprintf(buf,"relay on %s\r",RelModemOutToIDmm);  //3
    send_com(NUMATO5, buf);
	DelayWithEventProcessing(0.5);
    sprintf(buf,"relay on %s\r",RelModemOutToVAlim); //TR
    send_com(NUMATO5, buf);
    DelayWithEventProcessing(WaitIvalStableOnDMM);								  

    //Perform current   ,measurement.
    value= (float)1e+3 * readDMM(0); //in mA

    //Reset relays  voltage measurement
//Lang:Danger
//relay off 3: micro coupure
//si relay 3 ferme +lentement que relay2, risque de CC Alim_V+ au GND via DMM,rly2,rly1
    sprintf(buf,"relay off %s\r",RelModemOutToVAlim);
    send_com(NUMATO5, buf);
	DelayWithEventProcessing(0.5);
    sprintf(buf,"relay off %s\r", RelModemOutToIDmm);
    send_com(NUMATO5, buf);
    sprintf(buf,"relay off %s\r", RelIn0VDMMToVAlim);
    send_com(NUMATO5, buf);
    return (float)value;
}

//---------------------------------------------------------------------------

float dmm_meas_volt_relay(char relay[])
{
    float measure=0;
    char command1[100]="";
    sprintf(command1,"relay on %s\r", relay);
    send_com(NUMATO5, command1);

    measure=readDMM(1);

    sprintf(command1,"relay off %s\r", relay);
    send_com(NUMATO5, command1);
    return (float)measure;
}

//---------------------------------------------------------------------------
float set_dmm_meas_diode(void)
{
	char 	buf[30]="";
	//For AMM01 
	sprintf(buf,"relay on %s\r", RelInVDMMToGnd);
	send_com(NUMATO5, buf);
	sprintf(buf,"relay on %s\r", RelIn0VDmmToTp);
	send_com(NUMATO5, buf);
/*
	sprintf(buf,"relay off %s\r", RelInVDMMToGnd);
	send_com(NUMATO5, buf);
	sprintf(buf,"relay off %s\r", RelIn0VDmmToTp);
	send_com(NUMATO5, buf);
*/
	send_com(DMM,"FUNCTION DIODE\r");
    return 0;
}

//---------------------------------------------------------------------------
float reset_dmm_meas_diode(void)
{
	char 	buf[30]="";
	sprintf(buf,"relay off %s\r", RelInVDMMToGnd);
	send_com(NUMATO5, buf);
	sprintf(buf,"relay off %s\r", RelIn0VDmmToTp);
	send_com(NUMATO5, buf);
    send_com(DMM,"FUNCTION VOLT:DC\r");
    return 0;
}
//---------------------------------------------------------------------------
/// Initialize CVI's external test hardware

int CVI_TestInit(void)
{
		
    if (com11_open() != 0)
	{
		LogString("Com11 cannot be openned\r");
		return -1;
	}
	if (com12_open() != 0)
	{
		LogString("Com12 cannot be openned\r");
		return -1;
	}
    if (com13_open() != 0)
	{
		LogString("Com13 cannot be openned\r");
		return -1;
	}
    if (com14_open() != 0)
	{
		LogString("Com14 cannot be openned\r");
		return -1;
	}
    if (com15_open() != 0)
	{
		LogString("Com15 cannot be openned\r");
		return -1;
	}
    if (com16_open() != 0)
	{
		LogString("Com16 cannot be openned\r");
		return -1;
	}
    //if (com17_open() != 0)      return -1;
    if (com18_open() != 0)
	{
		LogString("Com18 cannot be openned\r");
		return -1;
	}
    //if (com19_open() != 0)      return -1;

    return 0;
}
//---------------------------------------------------------------------------
/// Initialize CVI's external devices

int CVI_InitExternalDevices(void)
{
	char    responseA[100]="";
	char    responseB[100]="";
	char	buf[30]="";
	int retcode=0;
	
	/*
	//Alim DC : set out off and test answer
  	send_com(SRC_DC,"SOUT1\r");
    retcode= ComRdTerm (SRC_DC, responseA, 3, 0x0D);   // don't wait for \r !!
	if ((strncmp(responseA,"OK",100) <0) || (retcode < 0))
	{
		sprintf(DeviceInError,"SRC_DC");
		return -1;
	}
	sprintf(buf,"VOLT045\r");  //U out = 4.5V
    send_com(SRC_DC, buf);
	DelayWithEventProcessing(WaitUAlimStable);
 	//Alim DC adjust current limit
	sprintf(buf,"CURR200\r");  //I out max = 2A
    send_com(SRC_DC, buf);
	*/


	//DMM
	SetComTime(DMM,3);
	if (send_com(DMM,"*IDN?\r") == -1)
	{
		LogString("DMM not connected");
		return -1;
	}
	else
	{
	    retcode= ComRdTerm (DMM, responseB, 50, 0x0A);   // don't wait for \r !!
		if ((strstr(responseB,"2831E") == NULL) || (retcode < 0))
		{
			LogString("Cannot found DMM");
			return -1;
		}
		send_com(DMM,":DISPLAY:ENABLE 1;:TRIG:SOURCE IMMEDIATE\r");   //Defaut, immediate measurement  
	}

	//Modem
	SetComTime(MODEM,3);
	if (send_com(MODEM,"RAZ\r") == -1)
	{
		LogString("MODEM not connected");
		return -1;
	}
	else
	{
		ProcessSystemEvents ();
	    retcode= ComRdTerm (MODEM, responseB, 50, 0x0A);   // don't wait for \r !!
		if ((strstr(responseB,"RADICOS Technologie") == NULL) || (retcode < 0))
		{
			LogString("Cannot found MODEM");
			return -1;
		}
		else
		{
			DelayWithEventProcessing(2);   //Wait all message received
			FlushInQ  (MODEM);
			ProcessSystemEvents ();
			send_com(MODEM,"par,7,5\r");   //Set modem NB scan nodes = 7
			ProcessSystemEvents ();
			if (CheckCmdExecOK() == 0)
			{
				FlushInQ  (MODEM);
				send_com(MODEM,"wpar\r");   //Save par
				DelayWithEventProcessing(4);
				if (CheckCmdExecOK() == 0)
				{
					FlushInQ  (MODEM);
					send_com(MODEM,"mcf,9,0\r");   //Set modem to send data only after RMEAS cmd
					if (CheckCmdExecOK() != 0)
					{
						LogString("Cannot set mcf 9=0");
						return -1;
					}
				}
			}
			else
			{
				LogString("Cannot init MODEM");
				return -1;
			}
		}
	}

	//STLINK
	
	
	
	return 0;
	
}

//---------------------------------------------------------------------------
/// Shutdown CVI's external test hardware

int CVI_TestCleanUp(void)
{
    send_com(NUMATO1, "reset\r");
    send_com(NUMATO2, "reset\r");
    send_com(NUMATO3, "reset\r");
    send_com(NUMATO4, "reset\r");
    send_com(NUMATO5, "reset\r");
	
	
	
	com_close(com11_port);
    com_close(com12_port);
    com_close(com13_port);
    com_close(com14_port);
    com_close(com15_port);
    com_close(com16_port);
    com_close(com18_port);
	return 0;
}

//---------------------------------------------------------------------------
//
//Read the current until value ander max val or time out
float ReadCurrentUntilOK(char ResultStr[50])
{
	char	buf[50] = "";
	float		Status = -1;
	float	MesVal = 0;
	int 	NbCheck=0;
	const int	MaxCheck=50;
    					   
	do  //essayer plusieurs fois, peut r�pondre erreur � la premi�re demande
	{
		NbCheck++;
		DelayWithEventProcessing(0.1);
		MesVal=(float)1e+3 * readDMM(0); //in mA 
	} while (((MesVal > TestMaxVal[CurrTest]) || (MesVal < TestMinVal[CurrTest])) && (NbCheck <= MaxCheck));
	//Wait value stable
	DelayWithEventProcessing(0.2);
	Status=(float)1e+3 * readDMM(0); //in mA
	if (LogWithAddOnInfos)
		sprintf(ResultStr, "%s - %d", buf, NbCheck);
	return Status;
}

//--------------------------------------------------------------------------
//
//Replace char befor . by a char represent the posiiton of the board tested 
int	ReplaceIDinName(char *inputstring, char *outputstring)
{
	int inputPtr, outputPtr = 0, charPosition;
	char toreplace;
	charPosition = strlen(inputstring) - 5;
	// walk though the input string
	for (inputPtr = 0; inputPtr < strlen(inputstring); inputPtr++, outputPtr++)
	{
		// search for the ASCII backspace character that's part of the spinner
		if (inputPtr == charPosition)
		{
			outputstring[outputPtr] = NodeId[0];
		}
		else
		{
			outputstring[outputPtr] = inputstring[inputPtr];
		}
	}
	outputstring[outputPtr] = '\0';
	return 0;
}


//---------------------------------------------------------------------------
//
//Load firmware in ST8
int LoadFirmware(char ResultStr[50],char Firmware[30])
{
	char 	buf[200]="";
	char	CmdLine[250]="";  
	int		ResOpenFile=-1;
	int		NbReadChar=0;
	char	CharToTest[2000]="";
	char	ResultFile[200];
	int		FileExist;
	int		Status =-1;
	int 	CmdStatus = -1;
	ssize_t		SizeOfFile;
	char	*PrtSt=NULL;
	int 	NbCheck=0;
	const int	MaxCheck=4;
	char	FirmwareName [30];

	
	//Effacer si il existe l'ancien fichier du r�sultat de la programation du firmware
	sprintf(ResultFile,"./%s",ProgCmdLogFileName); 
	FileExist=GetFileInfo(ResultFile,&SizeOfFile);
	if (FileExist!=0)
	{
		RemoveFileIfExists(ResultFile);
	}

	//Prepare the right file name in function of the position of the board in tester and the phase
	//0 to 8 or 9 to F
	ReplaceIDinName(Firmware,FirmwareName); 
	
	do  //essayer plusieurs fois, same time error during programming
	{
		NbCheck++;
		ProcessSystemEvents ();
		//sprintf(CmdLine,"CMD /A /C \"%s\\STM8_pgm TSTM8L152C6 Q E BC F%s\\%s V%s\\%s > Result.log",ProgCmdPath,FirmwarePath,FirmwareName,FirmwarePath,FirmwareName);	
		//sprintf(CmdLine,"CMD /A /C \"%s\\STM8_pgm TSTM8L152C6 E\rpause",ProgCmdPath);	
		sprintf(CmdLine,"CMD /A /C \"%s\\RLinkProg.bat %s %s %s",ProgCmdPath,FirmwarePath,FirmwareName,ProgCmdPath);	
		CmdStatus = system(CmdLine);   
		ProcessSystemEvents ();
		//Lire et interpreter le r�sultat
		ResOpenFile = OpenFile(ResultFile, VAL_READ_ONLY, VAL_OPEN_AS_IS , VAL_ASCII);
		if (ResOpenFile == -1)
		{
			sprintf(ResultStr,"impossible de lire le resultat"); 
		}
		else
		{
			NbReadChar = ReadFile(ResOpenFile,CharToTest,2000);
			if (NbReadChar <= 0)
			{
				sprintf(ResultStr,"Error during reading result");
				NbReadChar = 0;
			}
			PrtSt=strstr(CharToTest,"Flash contents matches file."); 
			if (PrtSt !=NULL)
			{
				Status =  0;
				sprintf(ResultStr,"Prog STM8 OK");
				NbReadChar = 0;
			}
			else
			{
				sprintf(ResultStr,"Error during prog.");
			}
		}
		if ( NbCheck >= MaxCheck)
		{
			break;
		}
	} while ( Status !=0);
	if (NbCheck > 1)
	{
		sprintf(ResultStr,"%s - Retry %d", ResultStr, NbCheck);
	}
	else
	{
		sprintf(ResultStr,"%s", ResultStr);
	}

	return Status;
}

//**************************
//
//Attention: CVI_Testxx() can be executed two time in case of error during
//           the first execustion.
//			 Be shure the the procedure can be re-executed
//
//*************************
//---------------------------------------------------------------------------
/// reset test hardware here

int CVI_Test0(void)
{
    return ERR_NONE;
}

//---------------------------------------------------------------------------
/// check if all probe are in contact AND NOT IN c.c
// check  TP_RS
int CVI_Test1(void)
{
	char 	buf[30]="";
	SetFirstVisibleRow(1);
	set_dmm_meas_diode();
	sprintf(buf,"relay on %s\r", RelMesRs);
	send_com(NUMATO5, buf);
    measurement=readDMM(0);
	sprintf(buf,"relay off %s\r", RelMesRs);
	send_com(NUMATO5, buf);
    TestMeasVal[CurrTest] = measurement;
    return ERR_NONE;
}
//---------------------------------------------------------------------------
// check  TP_RVO
int CVI_Test2(void)
{
	char 	buf[30]="";
	sprintf(buf,"relay on %s\r", RelMesRvo);
	send_com(NUMATO5, buf);
    measurement=readDMM(0);
	sprintf(buf,"relay off %s\r", RelMesRvo);
	send_com(NUMATO5, buf);
    TestMeasVal[CurrTest] = measurement;
    return ERR_NONE;
}
//---------------------------------------------------------------------------
// check TP_LS
int CVI_Test3(void)
{
	char 	buf[30]="";
	sprintf(buf,"relay on %s\r", RelMesLs);
	send_com(NUMATO5, buf);
    measurement=readDMM(0);
	sprintf(buf,"relay off %s\r", RelMesLs);
	send_com(NUMATO5, buf);
    TestMeasVal[CurrTest] = measurement;
    return ERR_NONE;
}
//---------------------------------------------------------------------------
// check TP_Sres
int CVI_Test4(void)
{
	char 	buf[30]="";
	sprintf(buf,"relay on %s\r", RelMesSres);
	send_com(NUMATO5, buf);
    measurement=readDMM(0);
	sprintf(buf,"relay off %s\r", RelMesSres);
	send_com(NUMATO5, buf);
    TestMeasVal[CurrTest] = measurement;
    return ERR_NONE;
}
//---------------------------------------------------------------------------
// check TP_Swim
int CVI_Test5(void)
{
	char 	buf[30]="";
	sprintf(buf,"relay on %s\r", RelMesSwim);
	send_com(NUMATO5, buf);
    measurement=readDMM(0);
	sprintf(buf,"relay off %s\r", RelMesSwim);
	send_com(NUMATO5, buf);
    TestMeasVal[CurrTest] = measurement;
    return ERR_NONE;
}
//---------------------------------------------------------------------------
// check TP_Sei
int CVI_Test6(void)
{
	char 	buf[30]="";
	sprintf(buf,"relay on %s\r", RelMesVsei);
	send_com(NUMATO5, buf);
    measurement=readDMM(0);
	sprintf(buf,"relay off %s\r", RelMesVsei);
	send_com(NUMATO5, buf);
    TestMeasVal[CurrTest] = measurement;
    return ERR_NONE;
}
//---------------------------------------------------------------------------
// check TP_Vdd
int CVI_Test7(void)
{
	char 	buf[30]="";
	sprintf(buf,"relay on %s\r", RelMesVdd);
	send_com(NUMATO5, buf);
    measurement=readDMM(0);
	sprintf(buf,"relay off %s\r", RelMesVdd);
	send_com(NUMATO5, buf);
    TestMeasVal[CurrTest] = measurement;
    return ERR_NONE;
}
//---------------------------------------------------------------------------
// check TP_ScOut
int CVI_Test8(void)
{
	char 	buf[30]="";
	sprintf(buf,"relay on %s\r", RelMesScOut);
	send_com(NUMATO5, buf);
    measurement=readDMM(0);
	sprintf(buf,"relay off %s\r", RelMesScOut);
	send_com(NUMATO5, buf);
    TestMeasVal[CurrTest] = measurement;
    return ERR_NONE;
}
//---------------------------------------------------------------------------
// check TP_Oos
int CVI_Test9(void)
{
	char 	buf[30]="";
	sprintf(buf,"relay on %s\r", RelMesOos);
	send_com(NUMATO5, buf);
    measurement=readDMM(0);
	sprintf(buf,"relay off %s\r", RelMesOos);
	send_com(NUMATO5, buf);
    TestMeasVal[CurrTest] = measurement;
    return ERR_NONE;
}
//---------------------------------------------------------------------------
// check TP_LGRD
int CVI_Test10(void)
{
	char 	buf[30]="";
	sprintf(buf,"relay on %s\r", RelMesLGRD);
	send_com(NUMATO5, buf);
    measurement=readDMM(0);
	sprintf(buf,"relay off %s\r", RelMesLGRD);
	send_com(NUMATO5, buf);
    TestMeasVal[CurrTest] = measurement;
	return ERR_NONE;
}
//---------------------------------------------------------------------------
// check TP_LGRS
int CVI_Test11(void)
{
	char 	buf[30]="";
	sprintf(buf,"relay on %s\r", RelMesLGRS);
	send_com(NUMATO5, buf);
    measurement=readDMM(0);
	sprintf(buf,"relay off %s\r", RelMesLGRS);
	send_com(NUMATO5, buf);
    TestMeasVal[CurrTest] = measurement;
	return ERR_NONE;
}
//---------------------------------------------------------------------------
//Check Rvo before loading firmware
int CVI_Test12(void)
{
	char 	buf[30]="";
	reset_dmm_meas_diode();
	set_volt_measurement();
	sprintf(buf,"relay on %s\r", RelModemToRS);
	send_com(NUMATO5, buf);
	sprintf(buf,"ADD,A\r");
	send_com(MODEM, buf);
	DelayWithEventProcessing(WaitAddFirstTime);
	TestMeasVal[CurrTest]=dmm_meas_volt_relay(RelMesRvo);
	return ERR_NONE;
}

//---------------------------------------------------------------------------
//Programm STM8 
int CVI_Test13(void)
{
	char 	buf[30]="";
	//RVO allready on
	//and connect RAISONANCE programmer 
	sprintf(buf,"relay on %s\r", RelProgToGnd);
    send_com(NUMATO5, buf);
	sprintf(buf,"relay on %s\r", RelProgToRVO);
    send_com(NUMATO5, buf);
	sprintf(buf,"relay on %s\r", RelProgToSres);
    send_com(NUMATO5, buf);
	sprintf(buf,"relay on %s\r", RelProgToSwim);
    send_com(NUMATO5, buf);

	TestMeasVal[CurrTest] = LoadFirmware(TestResultTxt[CurrTest],TestRemark[CurrTest]);

	//Unconnect Rlink
	sprintf(buf,"relay off %s\r", RelProgToGnd);
    send_com(NUMATO5, buf);
	sprintf(buf,"relay off %s\r", RelProgToRVO);
    send_com(NUMATO5, buf);
	sprintf(buf,"relay off %s\r", RelProgToSres);
    send_com(NUMATO5, buf);
	sprintf(buf,"relay off %s\r", RelProgToSwim);
    send_com(NUMATO5, buf);

	return ERR_NONE;
}
													 
//---------------------------------------------------------------------------
//Scan and check if node ID OK 
int CVI_Test14(void)
{
	char 	buf[50]="";
	//Reset node after prog
	sprintf(buf,"ADD,Z\r");
	send_com(MODEM, buf);
	DelayWithEventProcessing(WaitAddFirstTime);
	TestMeasVal[CurrTest]=CheckId(buf);
	sprintf(TestResultTxt[CurrTest],buf);
	return ERR_NONE;
}
													 
//---------------------------------------------------------------------------
//Stop Add High on Node 1, measure Ivdd
int CVI_Test15(void)
{
	char 	buf[30]="";
	sprintf(buf,"DRV,I\r");
	send_com(MODEM, buf);
	DelayWithEventProcessing(0.5);
	set_curr_measurement();
	//Already scanning
	sprintf(buf,"STOPAH,1\r");
	send_com(MODEM, buf);
	DelayWithEventProcessing(0.5);
	TestMeasVal[CurrTest]=readDMM(1);
	return ERR_NONE;
}

//---------------------------------------------------------------------------
//Stop ON on Node 1, measure Ivdd
int CVI_Test16(void)
{
	char 	buf[30]="";
	sprintf(buf,"DRV,R\r");
	send_com(MODEM, buf);
	DelayWithEventProcessing(1);
	sprintf(buf,"STOPON,1\r");
	send_com(MODEM, buf);
	DelayWithEventProcessing(2);
	TestMeasVal[CurrTest]=readDMM(1);
	return ERR_NONE;
}

//---------------------------------------------------------------------------
//Stop Add High on Node 2, measure Ivdd
int CVI_Test17(void)
{
	char 	buf[30]="";
	sprintf(buf,"DRV,R\r");
	send_com(MODEM, buf);
	DelayWithEventProcessing(0.2);
	sprintf(buf,"STOPAH,2\r");
	send_com(MODEM, buf);
	DelayWithEventProcessing(0.5);
	TestMeasVal[CurrTest]=readDMM(1);
	return ERR_NONE;
}

//---------------------------------------------------------------------------
//Check ouput power
int CVI_Test18(void)
{
	char 	buf[30]="";
	set_volt_measurement();
	TestMeasVal[CurrTest]=dmm_meas_volt_relay(RelMesLs);
	return ERR_NONE;
}
//---------------------------------------------------------------------------
//Stop on reset wait 1s measure RVO
int CVI_Test19(void)
{
	char 	buf[30]="";
	sprintf(buf,"DRV,R\r");
	send_com(MODEM, buf);
	DelayWithEventProcessing(0.2);
	sprintf(buf,"STOPRES\r");
	send_com(MODEM, buf);
	DelayWithEventProcessing(1);
	TestMeasVal[CurrTest]=dmm_meas_volt_relay(RelMesRvo);
	return ERR_NONE;
}

//---------------------------------------------------------------------------
//Stop Add High on Node 1, measure Uvdd
int CVI_Test20(void)
{
	char 	buf[30]="";
	sprintf(buf,"DRV,R\r");
	send_com(MODEM, buf);
	DelayWithEventProcessing(0.2);
	sprintf(buf,"STOPAH,1\r");
	send_com(MODEM, buf);
	DelayWithEventProcessing(0.2);
	TestMeasVal[CurrTest]=dmm_meas_volt_relay(RelMesVdd);
	return ERR_NONE ;
}

//---------------------------------------------------------------------------
//Stop Add High on Node 1, measure Usei
int CVI_Test21(void)
{
	TestMeasVal[CurrTest]=dmm_meas_volt_relay(RelMesVsei);
	return ERR_NONE ;
}

//---------------------------------------------------------------------------
//Check ouput power off 
int CVI_Test22(void)
{
	TestMeasVal[CurrTest]=dmm_meas_volt_relay(RelMesLs);
	TestMeasVal[CurrTest]=dmm_meas_volt_relay(RelMesLs);
	return ERR_NONE;
}

//---------------------------------------------------------------------------
//Stop on reset wait 2s measure Usei
int CVI_Test23(void)
{
	char 	buf[30]="";
	sprintf(buf,"DRV,R\r");
	send_com(MODEM, buf);
	DelayWithEventProcessing(1);
	SetFirstVisibleRow(CurrTest - 10);
	sprintf(buf,"STOPRES\r");
	send_com(MODEM, buf);
	DelayWithEventProcessing(2);
	TestMeasVal[CurrTest]=dmm_meas_volt_relay(RelMesVsei);
	return ERR_NONE;
}

//---------------------------------------------------------------------------
//Scan from Right side and check if scan
int CVI_Test24(void)
{
	char 	buf[50]="";
	SetFirstVisibleRow(CurrTest - 10);
	TestMeasVal[CurrTest]=CheckScanRaw(buf);
	sprintf(TestResultTxt[CurrTest],buf);
	return ERR_NONE;
}

//---------------------------------------------------------------------------
//Stop Add High on Node 1, measure Ivdd  From left side
int CVI_Test25(void)
{
	char 	buf[50]="";
	char	ptr[10]="";
	int		retcodeA=-1;
	set_curr_measurement();
	
	//Stop scan
	sprintf(ptr,"ADD,Z\r");
	FlushInQ  (MODEM);
	retcodeA=ComWrt(MODEM, ptr, strlen(ptr));
	sprintf(buf,"relay off %s\r", RelModemToRS);
	send_com(NUMATO5, buf);
	sprintf(buf,"relay on %s\r", RelModemToLS);
	send_com(NUMATO5, buf);
	sprintf(ptr,"DRV,S\r");
	FlushInQ  (MODEM);
	retcodeA=ComWrt(MODEM, ptr, strlen(ptr));
	DelayWithEventProcessing(1);
	//scanning
	sprintf(buf,"STOPAH,1\r");
	send_com(MODEM, buf);
	DelayWithEventProcessing(1.5);
	TestMeasVal[CurrTest]=readDMM(1);
	return ERR_NONE;
}

//---------------------------------------------------------------------------
//Stop ON on Node 1, measure Ivdd
int CVI_Test26(void)
{
	char 	buf[30]="";
	sprintf(buf,"DRV,R\r");
	send_com(MODEM, buf);
	DelayWithEventProcessing(0.2);
	sprintf(buf,"STOPON,1\r");
	send_com(MODEM, buf);
	DelayWithEventProcessing(1.5);
	TestMeasVal[CurrTest]=readDMM(1);
	return ERR_NONE;
}

//---------------------------------------------------------------------------
//Stop Add High on Node 2, measure Ivdd
int CVI_Test27(void)
{
	char 	buf[30]="";
	sprintf(buf,"DRV,R\r");
	send_com(MODEM, buf);
	DelayWithEventProcessing(0.2);
	sprintf(buf,"STOPAH,2\r");
	send_com(MODEM, buf);
	DelayWithEventProcessing(0.5);
	TestMeasVal[CurrTest]=readDMM(1);
	return ERR_NONE;
}

//---------------------------------------------------------------------------
//Check ouput power
int CVI_Test28(void)
{
	char 	buf[30]="";
	set_volt_measurement();
	TestMeasVal[CurrTest]=dmm_meas_volt_relay(RelMesLs);
	return ERR_NONE;
}
//---------------------------------------------------------------------------
//Stop on reset wait 1s measure RVO
int CVI_Test29(void)
{
	char 	buf[30]="";
	sprintf(buf,"DRV,R\r");
	send_com(MODEM, buf);
	DelayWithEventProcessing(0.2);
	sprintf(buf,"STOPRES\r");
	send_com(MODEM, buf);
	DelayWithEventProcessing(1);
	TestMeasVal[CurrTest]=dmm_meas_volt_relay(RelMesRvo);
	return ERR_NONE;
}

//---------------------------------------------------------------------------
//Stop Add High on Node 1, measure Uvdd
int CVI_Test30(void)
{
	char 	buf[30]="";
	sprintf(buf,"DRV,R\r");
	send_com(MODEM, buf);
	DelayWithEventProcessing(0.2);
	sprintf(buf,"STOPAH,1\r");
	send_com(MODEM, buf);
	DelayWithEventProcessing(0.2);
	TestMeasVal[CurrTest]=dmm_meas_volt_relay(RelMesVdd);
	return ERR_NONE ;
}

//---------------------------------------------------------------------------
//Stop Add High on Node 1, measure Usei
int CVI_Test31(void)
{
	char 	buf[30]="";
	sprintf(buf,"DRV,R\r");
	send_com(MODEM, buf);
	DelayWithEventProcessing(0.2);
	sprintf(buf,"STOPAH,1\r");
	send_com(MODEM, buf);
	DelayWithEventProcessing(0.2);
	TestMeasVal[CurrTest]=dmm_meas_volt_relay(RelMesVsei);
	return ERR_NONE ;
}

//---------------------------------------------------------------------------
//Check ouput power off 
int CVI_Test32(void)
{
	char 	buf[30]="";
    //Ckear charge at input
	sprintf(buf,"relay on %s\r", RelInVDMMToGnd);
    send_com(NUMATO5, buf);
	sprintf(buf,"relay off %s\r", RelInVDMMToGnd);
    send_com(NUMATO5, buf);
	
	TestMeasVal[CurrTest]=dmm_meas_volt_relay(RelMesRs);
	return ERR_NONE;
}

//---------------------------------------------------------------------------
//Stop on reset wait 2s measure Usei
int CVI_Test33(void)
{
	char 	buf[30]="";
	sprintf(buf,"DRV,R\r");
	send_com(MODEM, buf);
	DelayWithEventProcessing(0.2);
	sprintf(buf,"STOPRES\r");
	send_com(MODEM, buf);
	DelayWithEventProcessing(2);
	TestMeasVal[CurrTest]=dmm_meas_volt_relay(RelMesVsei);
	return ERR_NONE;
}

//---------------------------------------------------------------------------
//Scan from left side and check if scan
int CVI_Test34(void)
{
	char 	buf[50]="";
	TestMeasVal[CurrTest]=CheckScanRaw(buf);
	sprintf(TestResultTxt[CurrTest],buf);
	return ERR_NONE;
}

//---------------------------------------------------------------------------
//
int CVI_Test35(void)
{
	return ERR_NONE ;
}
//---------------------------------------------------------------------------
//
int CVI_Test36(void)
{
    return ERR_NONE;
}
//---------------------------------------------------------------------------
//
int CVI_Test37(void)
{
	return ERR_NONE ;
}
//---------------------------------------------------------------------------
//
int CVI_Test38(void)
{
	return ERR_NONE;
}

//---------------------------------------------------------------------------
//
int CVI_Test39(void)
{
	return ERR_NONE;
}

//---------------------------------------------------------------------------
//
int CVI_Test40(void)
{
	return ERR_NONE;
}

//---------------------------------------------------------------------------
//
int CVI_Test41(void)
{
	return ERR_NONE;
}

//---------------------------------------------------------------------------
//Used to replace "," by "." to not create a new cell in CSV file
//replace \r and \n by space
void strreplace(char Txt[150], char chr, char repl_chr)
{
	int i=0;
    while(Txt[i]!='\0')
	 {
	       if(Txt[i]==chr)
	       {
	           Txt[i]=repl_chr;
	       }  
	       if ((Txt[i]=='\r') || (Txt[i]=='\n'))
	       {
	           Txt[i]=0x20;
	       }  
	       i++; 
	 }
}

//---------------------------------------------------------------------------

__logResult(void)
{
    int     i;
    char    testRes[40];
    time_t  rawtime;
    struct tm *info;

    time (&rawtime);
    info = localtime(&rawtime);
    sprintf(fullRes, "%02d%02d%02d,%02d%02d%02d,",
                    info->tm_year+1900-2000, info->tm_mon+1, info->tm_mday,
                    info->tm_hour, info->tm_min, info->tm_sec);

    if (AllTestsResult == TEST_PASSED)
    {
        strcat (fullRes, "Pass");
    }
    else
    {
        strcat (fullRes, "Fail");
    }

    sprintf(testRes, ",%06u,%d", sn,BoardInProcess);
    strcat (fullRes, testRes);

	for (i = 1; i <= MAX_TEST; i++)
    {
		if (TestResult[i] == ERR_SKIPPED)
		{
			sprintf(fullRes,"%s,Skipped",fullRes);
		}
		else
		{
			if ((TestMinVal[i] >= -1E-12) && (TestMaxVal[i] <= 1E-12))
			{
				strreplace(TestResultTxt[i],0x2c,0x2e);  //To not insert a CSV char
				sprintf(fullRes,"%s,%s",fullRes,TestResultTxt[i]);
			}
			else
			{
				if ( TestMeasVal[i] >= 0.01)
				{
					sprintf(testRes, ",%0.3f", TestMeasVal[i]);
				}
				else
				{
					sprintf(testRes, ",%.*e", 2, TestMeasVal[i]);
					
				}
				strcat (fullRes, testRes);
				if (LogWithAddOnInfos) // save "retry" if retry
				{
					strreplace(TestResultTxt[i],0x2c,0x2e);  //To not insert a CSV char
					sprintf(fullRes,"%s_%s",fullRes,TestResultTxt[i]);
				}
			}
			if (LogWithAddOnInfos)
			{
				sprintf(testRes, " - %0.3f", TestTime[i]);
				strcat (fullRes, testRes);
			}
		}
    }
	
	sprintf(fullRes, "%s,%s", fullRes,AllTestInError);

    LogString(fullRes);
    return 0;
}

/////////////////////////////////////////////////////////////////////////////
///  Automatically chain all tests
///  Not for the faint of heart: heavy use of function pointers

const Ptr2Func  TestFunction[MAX_TEST+1] = {
    0,       // add 1 dummy element to start with index=1
    &CVI_Test1,  &CVI_Test2,  &CVI_Test3, &CVI_Test4,  &CVI_Test5,  
	&CVI_Test6,  &CVI_Test7,  &CVI_Test8,  &CVI_Test9,  &CVI_Test10,
    &CVI_Test11, &CVI_Test12, &CVI_Test13, &CVI_Test14, &CVI_Test15,
    &CVI_Test16, &CVI_Test17, &CVI_Test18, &CVI_Test19, &CVI_Test20,
	&CVI_Test21, &CVI_Test22, &CVI_Test23, &CVI_Test24, &CVI_Test25,
	&CVI_Test26, &CVI_Test27, &CVI_Test28, &CVI_Test29, &CVI_Test30,
	&CVI_Test31, &CVI_Test32, &CVI_Test33, &CVI_Test34, &CVI_Test35,
	&CVI_Test36, &CVI_Test37, &CVI_Test38, &CVI_Test39, &CVI_Test40,
	&CVI_Test41
	
	/*
	, &CVI_Test42, &CVI_Test43, &CVI_Test44, &CVI_Test45
	*/
};


//---------------------------------------------------------------------------

void Reset_All(void)
{
	char 	buf[30]="";
	//Modem off	
	sprintf(buf,"ADD,Z\r");
	send_com(MODEM, buf);
	//Reset all switches
	send_com(NUMATO1, "reset\r");
	send_com(NUMATO2, "reset\r");
	send_com(NUMATO3, "reset\r");
	send_com(NUMATO4, "reset\r");
	send_com(NUMATO5, "reset\r");
}

//---------------------------------------------------------------------------

int CVI_TestAll(void)
{
    int i;
	double DeltaVal;
	//Make a retry only when value out of bond.  Make one retry
	const int	MaxRetry =1;
	char WithRetry[10];
	

    /// clear all errors
    Init_TestSummary();
    Clear_TestResults();
	ProcessSystemEvents();
	
    for(i = 0; i <= MAX_TEST; i++)
    {
        TestResult		[i] = ERR_NOT_INIT;
 		TestMeasVal		[i] = -1;
		TestResultTxt	[i][0] = 0;
		TestTime	    [i] = 0;

   }

    /// now launch all tests, stop at 1st fatal error
    CurrTest = 1;
    LogFileEnable  = 1;
	DoRetry = 0;

	GetCurrentCVIAbsoluteTime (&TheOldTime);
    do
    {
        sprintf(FirstValInError, "");
		/// - Can run this test?
        if (TestState[CurrTest] >= TEST_NONFATAL)
        {
            /// - if so, do it (call using function pointer)
            if (TestFunction[CurrTest]() == ERR_NONE)   // test was executed?
            {
                // so check for result's detail
                if ((TestMeasVal[CurrTest] >= TestMinVal[CurrTest])
					&& (TestMeasVal[CurrTest] <= TestMaxVal[CurrTest]) )
				{
                    TestResult[CurrTest] = ERR_NONE;
					DoRetry = 0;	//after retry good test => Reset retry
				}
                else
				{
                    TestResult[CurrTest] = ERR_NONFATAL;
					DoRetry++;
					sprintf(WithRetry,"- Retry");
				}
            }
			else
			{
				DoRetry = 0;
				//Test not return ERR_NONE ???	
			}
        }
		else if (TestState[CurrTest] == TEST_SKIPPED)   
		{
			TestResult[CurrTest] = ERR_SKIPPED;
			DoRetry = 0;    
		}
        if ((DoRetry ==0) || (DoRetry > MaxRetry))
		{
			DoRetry = 0;	//Reset is incremented
			sprintf(TestResultTxt[CurrTest],"%s%s", TestResultTxt[CurrTest], WithRetry);
			Update_TestResult(CurrTest);
	        ProcessSystemEvents();
			if (LogWithAddOnInfos)
			{
				GetCurrentCVIAbsoluteTime (&TheTime);
				SubtractCVIAbsoluteTimes(TheTime,TheOldTime, &DeltaTime);
				if ( CVITimeIntervalToSeconds (DeltaTime , &DeltaVal) < 0)
				{
					//Erreur de convertion
					TestTime[CurrTest] = 999;
				}
				else
				{
					TestTime[CurrTest] = DeltaVal;
				}
				TheOldTime = TheTime;  
			}
	        if (AbortTest != 0)
			{
				AllTestsResult = TEST_ABORTED;	
				Reset_All();
	            CurrTest = MAX_TEST+1;
			}
			else if (TestResult[CurrTest] == ERR_NONFATAL)
			{
				/// - and check for result of each test
	            switch (TestState[CurrTest])
	            {
					case TEST_SKIPPED:
						break;
					case TEST_NONFATAL:
						break;
		            case TEST_FATAL:
						Reset_All();
		            	CurrTest = MAX_TEST+1;
						break;
		            case TEST_SKIPPED_NOMINMAX:
						break;
		            case TEST_NONFATAL_NOMINMAX:
						break;
		            case TEST_FATAL_NOMINMAX:   
						Reset_All();
		                CurrTest = MAX_TEST+1;
						break;
		            default:
						Reset_All();
		              	CurrTest = MAX_TEST+1; 
	            }
			}
			sprintf(WithRetry, "");
			CurrTest++;
		}
    } while (CurrTest <= MAX_TEST);

    /// And get the final verdict
    if (AllTestsResult != TEST_ABORTED)
	{
		AllTestsResult = TEST_PASSED;
	    for(i = 1; i <= MAX_TEST; i++)
			if ((TestResult[i] != ERR_NONE) && (TestResult[i] != ERR_SKIPPED))
			{	
	        	AllTestsResult = TEST_FAILED;
			}
	}
    Update_TestSummary();

    __logResult();

    return AllTestsResult;
}

//---------------------------------------------------------------------------

void SelectBoardForMeasurements(int BoardToSelect)
{
	char 	buf[30]="";
	send_com(NUMATO1, "reset\r");
	send_com(NUMATO2, "reset\r");
	send_com(NUMATO3, "reset\r");
	send_com(NUMATO4, "reset\r");
	send_com(NUMATO5, "reset\r");	//be shure not a relay on
	switch (BoardToSelect)
    {
		case 1:
			//Select 1rst module (for test)
			Fmt (buf, "%s<%s%s%s", "relay writeall ", SelectModule1_3_5_7, "\r" );
			send_com(NUMATO2, buf);
        break;
		case 2:
			//Select 1rst module (for test)
			Fmt (buf, "%s<%s%s%s", "relay writeall ", SelectModule2_4_6_8, "\r" );
			send_com(NUMATO2, buf);
        break;
		case 3:
			//Select 1rst module (for test)
			Fmt (buf, "%s<%s%s%s", "relay writeall ", SelectModule1_3_5_7, "\r" );
			send_com(NUMATO1, buf);
        break;
		case 4:
			//Select 1rst module (for test)
			Fmt (buf, "%s<%s%s%s", "relay writeall ", SelectModule2_4_6_8, "\r" );
			send_com(NUMATO1, buf);
        break;
		case 5:
			//Select 1rst module (for test)
			Fmt (buf, "%s<%s%s%s", "relay writeall ", SelectModule1_3_5_7, "\r" );
			send_com(NUMATO4, buf);
        break;
		case 6:
			//Select 1rst module (for test)
			Fmt (buf, "%s<%s%s%s", "relay writeall ", SelectModule2_4_6_8, "\r" );
			send_com(NUMATO4, buf);
        break;
		case 7:
			//Select 1rst module (for test)
			Fmt (buf, "%s<%s%s%s", "relay writeall ", SelectModule1_3_5_7, "\r" );
			send_com(NUMATO3, buf);
        break;
		case 8:
			//Select 1rst module (for test)
			Fmt (buf, "%s<%s%s%s", "relay writeall ", SelectModule2_4_6_8, "\r" );
			send_com(NUMATO3, buf);
        break;
    }
	
}

/////////////////////////////////////////////////////////////////////////////

int CVICALLBACK Wash_Relays (int panel, int control, int event,
							 void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
				send_com(NUMATO1, "relay writeall ffffffff\r");
				send_com(NUMATO1, "reset\r");
				send_com(NUMATO2, "relay writeall ffffffff\r");
				send_com(NUMATO2, "reset\r");
				send_com(NUMATO3, "relay writeall ffffffff\r");
				send_com(NUMATO3, "reset\r");
				send_com(NUMATO4, "relay writeall ffffffff\r");
				send_com(NUMATO4, "reset\r");
				send_com(NUMATO5, "relay writeall ffffffff\r");
 				send_com(NUMATO5, "reset\r");

			break;
	}
	return 0;
}

