/////////////////////////////////////////////////////////////////////////////
//
//  Project: CVI_Test framework
//
//  Copyright L&V Design   - CH-1031 Mex
//
/////////////////////////////////////////////////////////////////////////////
/**
    @file TwinModem.c
    All TwinModem commands defined here\n
    \n
    History:\n
      191220: PhW: Started\n
*/

#include "toolbox.h"
#include <formatio.h>
#include <utility.h>
#include <ansi_c.h>
#include <rs232.h>
#include "common.h"

/*


*/
int ExtractDataDrvS(char inputstring[], int RetCode, int NodeNumber, int SensorVal, int AlarmStat,
					int OverHeatCnt, int UnderHeatCnt, char RxRawData[7], char RxChecksum[4],
				   	int RxMDState, int RxErrorCode, int RdNodeId, int NodeError);

//---------------------------------------------------------------------------
//
//Extract data received after a modem cmd "drv,s" : extract the 12 datas of first node (node in test)
//If no error found in the data (RxMDState==Ready + RxErrorCode==0 + NodeError==0) return SensorVal other return -1
//Normaly tester never below 0 degree celsius.
int ExtractDataDrvS(char inputstring[], int RetCode, int NodeNumber, int SensorVal, int AlarmStat,
					int OverHeatCnt, int UnderHeatCnt, char RxRawData[7], char RxChecksum[4],
				   	int RxMDState, int RxErrorCode, int RdNodeId, int NodeError)
{
	int inputPtr, field=0, TmpValPtr=0, Status=0;
	char	TmpVal[10];
	int	ToTest;
	// walk though the input string
	for (inputPtr = 0; inputPtr < strlen(inputstring); inputPtr++)
	{
		if ( inputstring[inputPtr] == 44)
		{
			TmpVal[TmpValPtr] = '\0';
			switch (field)
			{
				case 0:
					ToTest = StrToInt(TmpVal, &RetCode);
					break;
				case 1:
					StrToInt(TmpVal, &NodeNumber);
					break;
				case 2:
					StrToInt(TmpVal, &SensorVal);
					break;
				case 3:
					StrToInt(TmpVal, &AlarmStat);
					break;
				case 4:
					StrToInt(TmpVal, &OverHeatCnt);
					break;
				case 5:
					StrToInt(TmpVal, &UnderHeatCnt);
					break;
				case 6:
					sprintf(RxRawData, TmpVal );
					break;
				case 7:
					sprintf(RxChecksum, TmpVal);
					break;
				case 8:
					StrToInt(TmpVal, &RxMDState);
					break;
				case 9:
					StrToInt(TmpVal, &RxErrorCode);
					break;
				case 10:
					StrToInt(TmpVal, &RdNodeId);
					break;
				case 11:
					StrToInt(TmpVal, &NodeError);
					break;
				default:
					break;
					
			}
			field++;
			TmpValPtr=0;
		}
		else
		{
			TmpVal[TmpValPtr] = inputstring[inputPtr];
			TmpValPtr++;
		}		
	}
	StrToInt(TmpVal, &NodeError);
	Status = SensorVal;
	if (RxMDState != 4)
	{
		Status = -1;
	}
	if (RxErrorCode != 0)
	{
		Status = -1;
	}
	if (NodeError != 0)
	{
		Status = -1;
	}
	return Status;
}



//---------------------------------------------------------------------------
//
//Check if a modem commad is corectly executed
//Modem must answer with a string with first char = "0" and 2nd char=',' or CR or LF
//If OK return 0 if not return -1
int CheckCmdExecOK()
{
	int Status=0;
	char	theresponse[30]="";
	int	retcode;
	//Read answer to cmd
	DelayWithEventProcessing(0.1);
	ProcessSystemEvents ();
	retcode= ComRdTerm (MODEM, theresponse, 20, 0x0D);
	ProcessSystemEvents ();
	if (retcode != -1)
	{
		// check firsrt and 2nd char
		if ( theresponse[0] == '0')
		{
			if ( strlen(theresponse) >= 2)
			{
				if (( theresponse[1] == ',') || ( theresponse[1] == '\r') || ( theresponse[1] == '\n')) 
				{
					Status = 0;
				}
				else
				{
					Status = -1;
				}
			}
			Status = 0;
		}
		else
		{
			Status = -1;
		}
	}
	else
	{
		LogString("MODEM no answer"); 
		Status = -1;
	}
		
	return Status;
}


//---------------------------------------------------------------------------
//
//If OK, return 0 with message in ResultStr, fail return -1 a fail message in ResultStr
int CheckId(char ResultStr[50])
{
    int 	retcodeA=0;
    int 	retcode=0;
    char 	response[100]="";
    char 	ptr[100]="";
	char	*PrtSt=NULL;
	int 	Len;
	char	Answear[100]="";
	int 	NbCheck=0;
 	const int	MaxCheck=3;
	int		Status=-1;
	char	buf[30];
    
	sprintf(ptr,"DRV,I\r");
	FlushInQ  (MODEM);
	retcodeA=ComWrt(MODEM, ptr, strlen(ptr));
	if (retcodeA > 0)
	{
	 	ProcessSystemEvents ();
		retcode= ComRdTerm (MODEM, response, 20, 0x0D);
	 	ProcessSystemEvents ();
		if (retcode > 0)
		{
			PrtSt=strstr(response,"0"); 
			if (PrtSt !=NULL)
			{
				DelayWithEventProcessing(0.4);
				sprintf(ptr,"RMEAS\r");
				FlushInQ  (MODEM);
				retcodeA=ComWrt(MODEM, ptr, strlen(ptr));
				DelayWithEventProcessing(1);
				memset(response,0,strlen(response));
				retcode= ComRdTerm (MODEM, response, 20, 0x0D);
			 	ProcessSystemEvents ();
				if (retcode > 0)
				{
					sprintf(buf,"24,%i", (BoardInProcess - 1 + (SerieIdToPrg *8)));
					PrtSt=strstr(response,buf); 
					if (PrtSt !=NULL)
					{
						sprintf(ResultStr,"Node ID %s OK", NodeId);
						Status= 0;  //return 0;
					}
					else
					{
						sprintf(buf,"24,");
						PrtSt=strstr(response,buf); 
						if (PrtSt !=NULL)
						{
							memset(ResultStr,0,strlen(ResultStr));
							Fmt(ResultStr,"Node ID return error %s", response);
						}
						else
						{
							sprintf(ResultStr,"RMEAS return nothing");
						}
					}
							
				}
				else 
				{	
					sprintf(ResultStr,"Unable to read modem answear, error code %s", PrtSt);
				}
			}
			else 
			{	
				sprintf(ResultStr,"DRV,I return error code %s", PrtSt);
			}
		}
		else
		{
			sprintf(ResultStr,"Modem ne repond pas");
		}
	}
	else
	{
		FlushOutQ(MODEM);
		sprintf(ResultStr,"Erreur d'envois de la com. au modem");
	}
	return Status;
}


//---------------------------------------------------------------------------
//
//If OK, return 0 with message in ResultStr, fail return -1 a fail message in ResultStr
int CheckScanRaw(char ResultStr[50])
{
    int 	retcodeA=0;
    int 	retcode=0;
    char 	responseA[100]="";
    char 	responseB[100]="";
    char 	ptr[100]="";
	char	*PrtSt=NULL;
	int 	Len;
	char	Answear[100]="";
	int 	NbCheck=0;
 	const int	MaxCheck=10;
	int		Status=-1;
	char	buf[30];
	int ACmdRetCode=0, ANodeNumber=0, ASensorVal=0, AAlarmStat=0, AOverHeatCnt=0, AUnderHeatCnt=0,
		ARxMDState=0, ARxErrorCode=0, ARdNodeId=0, ANodeError=0;
	char	ARxRawData[7], ARxChecksum[4]; 
    
	sprintf(ptr,"DRV,S\r");
	FlushInQ  (MODEM);
	retcodeA=ComWrt(MODEM, ptr, strlen(ptr));
	if (CheckCmdExecOK() == 0)
	{
		DelayWithEventProcessing(2);
		for (NbCheck=1; NbCheck <= MaxCheck; NbCheck++)
		{
			sprintf(ptr,"RMEAS\r");
			FlushInQ  (MODEM);
			retcodeA=ComWrt(MODEM, ptr, strlen(ptr));
			DelayWithEventProcessing(1);
			retcode= ComRdTerm (MODEM, responseB, 50, 0x0D);
		 	ProcessSystemEvents ();
			if (retcode > 0)
			{
				retcode = ExtractDataDrvS( responseB, ACmdRetCode, ANodeNumber, ASensorVal, AAlarmStat,
									 AOverHeatCnt, AUnderHeatCnt, ARxRawData, ARxChecksum,
			   						 ARxMDState, ARxErrorCode, ARdNodeId, ANodeError);
				if (retcode != -1)
				{
					Status =retcode;
				}
				else
				{
					sprintf(ResultStr,"Error scan, RxMDState:%i RxErrorCode%i NodeError%i", ARxMDState, ARxErrorCode, ANodeError);
					break;
				}
			}
			else 
			{	
				sprintf(ResultStr,"Unable to read modem answear, error code %s", PrtSt);
				break;
			}
		}
	}
	else
	{
		FlushOutQ(MODEM);
		sprintf(ResultStr,"Erreur d'envois de la com. au modem");
	}
	return Status;
}

//---------------------------------------------------------------------------
//
//If OK, return 0 with message in ResultStr, fail return -1 a fail message in ResultStr 
int SetOut18V(char ResultStr[50])
{
    int 	retcodeA=0;
    int 	retcode=0;
    char 	response[100]="";
    char 	ptr[100]="";
	char	*PrtSt=NULL;
	int 	NbCheck=0;
	int		Status=-1;
	int 	Len;
 	const int	MaxCheck=5;
   

	//Probablement commande � executer 2 x fois pour ne pas avvoir les donn�es du GPS qui polluent la r�ponse
	do
	{
		NbCheck++;
	    FlushInQ  (MODEM);
		sprintf(ptr,"$CNF*\r\n");
		retcodeA=ComWrt(MODEM, ptr, strlen(ptr));
		if (retcodeA > 0)
		{
		 	ProcessSystemEvents ();
			retcode= ComRdTerm (MODEM, response, 20, 0x0D);
		 	ProcessSystemEvents ();
			if (retcode > 0)
			{
				PrtSt=strstr(response,"$CNOK"); 
				if (PrtSt !=NULL)
				{
					sprintf(ResultStr,"GNS est off");
					Status= 0;
				}
			}
		}
		else
		{
			FlushOutQ(MODEM);
			Status = -1;
			sprintf(ResultStr,"GNS_Off erreur d'envois de la com.");
		}
		if  ((Status == -1) && (NbCheck == MaxCheck))
		{
			sprintf(ResultStr,"GNS_Off Mauvaise/ou pas de reponse du PIC");
			break;
		}
	} while (Status != 0);
	if (LogWithAddOnInfos)
		sprintf(ResultStr,"%s - %d", ResultStr, NbCheck);
	return Status;
}
//---------------------------------------------------------------------------
//
//If OK, return 0 with message in ResultStr, fail return -1 a fail message in ResultStr 
int SetOut0V(char ResultStr[50])
{
    int 	retcodeA=0;
    int 	retcode=0;
    char 	response[100]="";
    char 	ptr[100]="";
	char	*PrtSt=NULL;
	int 	NbCheck=0;
	int		Status=-1;
	int 	Len;
 	const int	MaxCheck=6;
   

	//Probablement commande � executer 2 x fois pour ne pas avoir les donn�es du GPS qui polluent la r�ponse
	do
	{
		NbCheck++;
		FlushInQ(MODEM);
		FlushOutQ(MODEM);
		sprintf(ptr,"$CNN*\r\n");
		retcodeA=ComWrt(MODEM, ptr, strlen(ptr));
	 	ProcessSystemEvents ();
		retcode= ComRdTerm (MODEM, response, 50, 0x0D);
	 	ProcessSystemEvents ();
		if (retcode > 0)
		{
			PrtSt=strstr(response,"$CNOK"); 
			if (PrtSt !=NULL)
			{
				sprintf(ResultStr,"GNS est on");
				Status= 0;
			}
			else
			{
				sprintf(ResultStr,"GNS_On Mauvaise de reponse du PIC"); 
			}
		}
		else
		{
			sprintf(ResultStr,"GNS_On Pas de reponse du PIC");
		}
		if  (NbCheck == MaxCheck)
		{
			sprintf(ResultStr,"GNS_On Mauvaise/ou pas de reponse du PIC");
			break;
		}
	} while (Status != 0);
	if (LogWithAddOnInfos)
		sprintf(ResultStr,"%s - %d", ResultStr, NbCheck);
	return Status;
}

//---------------------------------------------------------------------------
//
//If OK, return 0 with message in ResultStr, fail return -1 a fail message in ResultStr 
int SetOut9V(char ResultStr[50])
{
    int 	retcodeA=0;
    int 	retcode=0;
    char 	response[100]="";
    char 	ptr[100]="";
	char	*PrtSt=NULL;
	int 	NbCheck=0;
	int		Status=-1;
	int 	Len;
 	const int	MaxCheck=5;
   

	//Probablement commande � executer 2 x fois pour ne pas avvoir les donn�es du GPS qui polluent la r�ponse
	FlushInQ  (MODEM);
	do
	{
		NbCheck++;
		sprintf(ptr,"$CEF*\r\n");
		retcodeA=ComWrt(MODEM, ptr, strlen(ptr));
	 	ProcessSystemEvents();   //2016-01-14 avant ComRdTerm() 2x erreur sur 512tests
		retcode= ComRdTerm (MODEM, response, 50, 0x0D);
		ProcessSystemEvents();
		if (retcode > 0)
		{
			PrtSt=strstr(response,"$CEOK"); 
			if (PrtSt !=NULL)
			{
				sprintf(ResultStr,"Ge-866 est off");
				Status= 0;
			}
		}
		if  (NbCheck == MaxCheck)
		{
			sprintf(ResultStr,"Ge_Off Mauvaise/ou pas de reponse du PIC");
			break;
		}
	} while (Status != 0);
	if (LogWithAddOnInfos)
		sprintf(ResultStr,"%s - %d", ResultStr, NbCheck);
	return Status;
}
